# -*- coding: utf-8 -*-
import _thread
import cgitb
import importlib
import os
import random
import sys

from PyQt5.QtWidgets import QApplication
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.cron import CronTrigger

from backend.compoments.trayIcon import TrayIcon
from backend.compoments.workFillMainWindow import WorkFillMainWindow
from backend.service.workHourService import WorkHourService
from backend.util import configJsonUtil
from backend.util.globalContext import Context
from backend.util.holidayUtil import today_is_holiday
from backend.util.loggerFactory import Logger

logger = Logger()

VERSION = "1.0.0"


class App(object):

    def __init__(self):
        cgitb.enable(logdir="Log", format="text")
        os.putenv("QTWEBENGINE_REMOTE_DEBUGGING", "9223")
        os.putenv("QTWEBENGINE_CHROMIUM_FLAGS", "-single-process")
        app = QApplication(sys.argv)
        # 加载资源
        importlib.import_module("backend.resources.assert_rc")
        self.mainWindowImpl = WorkFillMainWindow(is_hidden=True)
        Context().mainWindowImpl = self.mainWindowImpl
        self.icon = TrayIcon(self.mainWindowImpl)
        Context().trayIcon = self.icon

        _thread.start_new_thread(self.start_work_hour_job, ())
        # _thread.start_new_thread(self.start_job, ())
        sys.exit(app.exec_())

    def start_work_hour_job(self):
        logger.info("启动工时填报任务")
        sched = BlockingScheduler()
        # 周一至周五下班开始任务
        config = configJsonUtil.get_config()
        hour_ = config['work_hour']['hour']
        hour = random.choice(hour_.split(','))
        minute_ = config['work_hour']['minute']
        minutes = minute_.split('-') if minute_.__contains__("-") else [minute_]
        minute = str(random.randint(int(minutes[0]), int(minutes[1]))) if len(minutes) > 1 else minutes[0]
        second = str(random.randint(0, 59))
        logger.info("当前填报执行时间:小时：{}， 分钟：{}，秒：{}".format(hour, minute, second))
        trigger = CronTrigger(hour=hour, minute=minute, second=second)
        sched.add_job(self.start_job, trigger)  # 根据需要进行设置
        sched.start()

    def start_job(self):
        try:
            logger.info("准备开始填报工时")
            # 如果是工作日才填报
            if not today_is_holiday():
                logger.info("工作日开始填报")
                workHourService = WorkHourService()
                workHourService.fill_work_hour_random()
                # days = workHourService.get_unfilled_workdays()
                # print(days)
                workHourService.audit_work_hour()
            else:
                logger.info("休息日不填报")
        except:
            import traceback
            logger.error(traceback.format_exc())


if __name__ == "__main__":
    App()
