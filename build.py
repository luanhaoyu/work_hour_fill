# -*- coding: utf-8 -*-
import os
import sys

from backend.util import argumentUtil, fileUtil

if not argumentUtil.has_sys_arg("-bWebpack=false"):
    os.chdir("frontend")
    print("npm run build")
    os.system("npm run build")
    os.chdir("..")
print("rd dist /s /q")
os.system("rd dist /s /q")
print("pyinstaller work_hour_fill.spec")
os.system("pyinstaller work_hour_fill.spec")

print("删除依赖")
# 可删依赖清单dll
files = ['Qt5Bluetooth.dll', 'Qt5Charts.dll', 'Qt5DBus.dll', 'Qt5Multimedia.dll', 'Qt5PositioningQuick.dll',
         'Qt5QmlWorkerScript.dll', 'Qt5Quick3D.dll', 'Qt5Quick3DAssetImport.dll', 'Qt5Quick3DRender.dll',
         'Qt5Quick3DRuntimeRender.dll', 'Qt5Quick3DUtils.dll', 'Qt5QuickControls2.dll', 'Qt5QuickParticles.dll',
         'Qt5QuickShapes.dll', 'Qt5QuickTemplates2.dll', 'Qt5QuickTest.dll', 'Qt5RemoteObjects.dll', 'Qt5Sensors.dll',
         'Qt5SerialPort.dll', 'Qt5Sql.dll', 'Qt5Svg.dll', 'Qt5Test.dll', 'Qt5XmlPatterns.dll']
targetFiles = list(
    map(lambda f: os.path.abspath(fileUtil.getRootPath() + '/dist/work_hour_fill/' + os.path.basename(f)), files))
for f in targetFiles:
    if os.path.exists(f):
        print("删除文件：{}".format(f))
        fileUtil.delFile(f)
sys.exit(0)
