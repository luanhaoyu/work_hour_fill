import Vue from 'vue';
import VueRouter, {RouteConfig} from 'vue-router';

Vue.use(VueRouter);

const routes: RouteConfig[] = [
    {
        path: '/programConfig',
        name: 'ProgramConfig',
        component: () => import(/* webpackChunkName: "projectConfig" */ '../views/ProgramConfig.vue'),
    },
    {
        path: '/',
        name: 'SysConfig',
        component: () => import(/* webpackChunkName: "sysConfig" */ '../views/SysConfig.vue'),
    },
    {
        path: '/accountConfig',
        name: 'accountConfig',
        component: () => import(/* webpackChunkName: "sysConfig" */ '../views/AccountConfig.vue'),
    },

];

const router = new VueRouter({
    // mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

export default router;
