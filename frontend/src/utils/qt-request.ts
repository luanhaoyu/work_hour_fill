declare let QWebChannel: any;

export class Resp {

    code: number = 0;
    msg: string = "";
    data: any = {};
}

export class QtHelper {

    defaultOptions: object = {
        path: "",
        callback: "",
        mode: "child",
        params: {}
    }

    private initChannel() {
        return new Promise((resolve, reject) => {
            if (window['qt']) {
                new QWebChannel(window['qt'].webChannelTransport, function (channel: any) {
                    resolve(channel)
                })
            } else {
                reject("非qt环境 无法获取qt对象")
            }
        })
    }

    public async request(path?: string, params?: object, options?: any): Promise<Resp> {
        let op: any = this.defaultOptions
        if (!options) {
            op = Object.assign(this.defaultOptions, options)
        }
        if (window['pyqtChannel'] == undefined) {
            let channel: any = await this.initChannel()
            window['pyqtChannel'] = channel.objects['pyqtChannel'];
            window['pyqtChannel']['callbacks'] = {}
        }
        let callbackKeys = Object.keys(window['pyqtChannel']['callbacks']);
        if (callbackKeys.length > 100) {
            window['pyqtChannel']['callbacks'] = {}
        }
        let randomCallbackName = "qtCallback_" + String(Math.random()).replace(".", "")
        return new Promise<Resp>((resolve, reject) => {
            window['pyqtChannel']['callbacks'][randomCallbackName] = function (res: any) {
                try {
                    console.dir(path + "，qt-request返回结果：" + res)
                    let data = res ? JSON.parse(res) : {code: '-1'};
                    let resp = new Resp();
                    resp.code = data.code
                    resp.msg = data.msg
                    resp.data = data.data
                    resolve(data)
                } catch (e) {
                    console.error(e)
                    reject(e)
                }
            }
            try {
                op.path = path
                op.params = params
                op.callback = "window.pyqtChannel.callbacks." + randomCallbackName
                let sOp = JSON.stringify(op);
                console.debug("qt-request请求：" + sOp)
                let qtService = window['pyqtChannel']['qt_service'];
                qtService(sOp)
            } catch (e) {
                reject(e)
                console.error(e)
            }
        })
    }

}
