import {QtHelper, Resp} from '@/utils/qt-request';

class Api {
  qtHelper = new QtHelper();
}

class SystemApi extends Api {


  public async getAccount(): Promise<Resp> {
    return await this.qtHelper.request('/system/get_account');
  }

  public async saveAccount(account: object): Promise<Resp> {
    return await this.qtHelper.request('/system/save_account', account);
  }

  public async resetAccount(): Promise<Resp> {
    return await this.qtHelper.request('/system/reset_account');
  }

  public async get_fill_config(): Promise<Resp> {
    return await this.qtHelper.request('/system/get_fill_config');
  }

  public async save_fill_config(fill_config: object): Promise<Resp> {
    return await this.qtHelper.request('/system/save_fill_config', fill_config);
  }

  public async get_version(): Promise<Resp> {
    return await this.qtHelper.request('/system/get_version');
  }

}

class ProgramApi extends Api {

  public async list(): Promise<Resp> {
    return await this.qtHelper.request('/program/list');
  }

  public async save(program: object): Promise<Resp> {
    return await this.qtHelper.request('/program/save', program);
  }

  public async del(id: string): Promise<Resp> {
    return await this.qtHelper.request('/program/del', {'id': id});
  }

  public async getMyProgram(): Promise<Resp> {
    return await this.qtHelper.request('/workHour/get_my_program');
  }
}

class WorkHourApi extends Api {

  public async fillWorkHour(day: string, work_hour: any): Promise<Resp> {
    return await this.qtHelper.request('/workHour/fill_work_hour', {
      day,
      work_hour
    });
  }

  public async getUnfilledWorkdays(start_time?: string, end_time?: any): Promise<Resp> {
    return await this.qtHelper.request('/workHour/get_unfilled_workdays', {
      'start_time': start_time,
      'end_time': end_time
    });
  }

}

const systemApi = new SystemApi();
const programApi = new ProgramApi();
const workHourApi = new WorkHourApi();

export {
  systemApi, programApi, workHourApi
};
