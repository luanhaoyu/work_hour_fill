import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import {
    Button,
    Container,
    Header,
    Main,
    Menu,
    MenuItem,
    Breadcrumb,
    BreadcrumbItem,
    Row,
    Col,
    Table,
    TableColumn,
    Form,
    FormItem,
    Dialog,
    Input,
    Message,
    MessageBox,
    Divider,
    Tooltip,
    Switch,
    Select,
    Option,
    DatePicker,
    Alert,
} from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import MyFromItem from "@/components/MyFromItem.vue";
Vue.use(Button);
Vue.use(Container);
Vue.use(Header);
Vue.use(Main);
Vue.use(Menu);
Vue.use(MenuItem);
Vue.use(Breadcrumb);
Vue.use(BreadcrumbItem);
Vue.use(Row);
Vue.use(Col);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Dialog);
Vue.use(Input);
Vue.use(Divider);
Vue.use(Tooltip);
Vue.use(Switch);
Vue.use(Select);
Vue.use(Option);
Vue.use(DatePicker);
Vue.use(Alert);

Vue.config.productionTip = false;
Vue.prototype.$message = Message;
Vue.prototype.$confirm = MessageBox.confirm;

new Vue({
    router,
    store,
    render: (h) => h(App),
    components:{
        MyFromItem
    }
}).$mount('#app');
