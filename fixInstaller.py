# -*- coding: utf-8 -*-
import os

env_dist = os.environ

# 拷贝pyinstaller补丁 pyinsataller 3.4
print("copy patch")

from backend.util import fileUtil

env_dist_get = env_dist.get('VIRTUAL_ENV')
if env_dist_get is None:
    env_dist_get = env_dist.get('PYTHONPATH')
print(env_dist_get)
fileUtil.copy_dir(fileUtil.getRootPath() + os.sep + "backend" + os.sep + "ext",
                  env_dist_get + os.sep + 'Lib' + os.sep + 'site-packages')
