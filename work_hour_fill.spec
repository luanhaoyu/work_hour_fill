# -*- mode: python -*-

block_cipher = None


a = Analysis(['main.py'],
             pathex=['.'],
             binaries=[],
             datas=[
                ("config.json","."),
                ('frontend/dist', 'resources'),
                ('library', '.'),
             ],
             hiddenimports=[
	          "backend.router.programRouter",
	          "backend.router.systemRouter",
	          "backend.router.workHourRouter",
	          "backend.router.testRouter",
	          "backend.resources.assert_rc",
	         ],
	         hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='work_hour_fill',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False,
          icon='backend/resources/image/work_hour_fill.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='work_hour_fill')
