# -*- coding: utf-8 -*-
from backend.exception.exceptions import BException
from backend.service.configJsonService import ConfigJsonService
from backend.service.ssoService import SSOService
from backend.util import configJsonUtil, aesUtil
from backend.util import startUpUtil
from backend.util.loggerFactory import Logger

logger = Logger()


class SystemService(ConfigJsonService):

    def __init__(self):
        ConfigJsonService.__init__(self)
        self.account_ = self.config['account']
        self.work_hour_ = self.config['work_hour']
        self.sso_service = None

    def get_account(self):
        user_id = self.account_['user_id']
        password = self.account_['password']
        user_name = self.account_['user_name']
        if password is None or password == '':
            decrypt_password = password
        else:
            decrypt_password = aesUtil.PrpCrypt.decrypt_password(user_id, password)
        return {
            "user_id": user_id,
            "user_name": user_name,
            "password": decrypt_password
        }

    def save_account(self, account: dict):
        user_id = account['user_id']
        password_input_text = account['password']
        if password_input_text is None or password_input_text == '':
            encrypt_password = ''
        else:
            encrypt_password = aesUtil.PrpCrypt.encrypt_password(user_id, password_input_text)
        self.account_['user_id'] = user_id
        self.account_['user_name'] = account['user_name']
        self.account_['password'] = encrypt_password
        configJsonUtil.save_json(self.config)

        if self.account_['user_id'] != '' and self.account_['user_id'] is not None:
            try:
                self.sso_service = SSOService(cache=False)
            except Exception as e:
                logger.error("账号保存异常:{}".format(e))
                raise BException("账号保存异常:{}".format(e))

    def reset_account(self):
        self.save_account({
            "user_id": "",
            "user_name": "",
            "password": ""
        })

    def get_fill_config(self):
        return self.work_hour_

    def save_fill_config(self, fill_config: dict):
        self.work_hour_['hour'] = fill_config['hour']
        self.work_hour_['minute'] = fill_config['minute']
        self.work_hour_['auto_start'] = fill_config['auto_start']
        if self.work_hour_['auto_start']:
            startUpUtil.create_shot_cut()
        else:
            startUpUtil.cancel_create_shot_cut()
        configJsonUtil.save_json(self.config)

    def get_version(self):
        from main import VERSION
        return VERSION
