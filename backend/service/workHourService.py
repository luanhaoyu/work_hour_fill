# -*- coding: utf-8 -*-
import datetime
import json
import random

from backend.exception.exceptions import BException
from backend.service.configJsonService import ConfigJsonService
from backend.service.ssoService import SSOService
from backend.util import configJsonUtil, aesUtil
from backend.util import httpUtil
from backend.util.loggerFactory import Logger

logger = Logger()


class WorkHourService(ConfigJsonService):

    def __init__(self, user_id=None, user_name=None, password=None):
        super().__init__()
        self.account = self.config['account']
        if user_id is None:
            self.user_id = self.account['user_id']
        else:
            self.user_id = user_id
        if user_name is None:
            self.user_name = self.account['user_name']
        else:
            self.user_name = user_name
        if password is None:
            account_password_ = self.account['password']
            if account_password_ is None or len(account_password_) == 0:
                raise OSError("密码不能为空，请进行账号配置")
            self.password = aesUtil.PrpCrypt.decrypt_password(self.account['user_id'], account_password_)
        else:
            self.password = password
        self.sso_service = SSOService(self.user_id, self.password)
        self.app_session_key = configJsonUtil.get_config()['base']['app_session_key']
        self.mod_auth_cas = self.sso_service.get_app_session_id()[self.app_session_key]

    def fill_work_hour_random(self, day=datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d'),
                              work_hour=None):
        # 默认获取当前时间
        if work_hour is None:
            programs = configJsonUtil.get_config()['programs']
            # 计算权重
            new_ps = []
            for p in programs:
                weight = p['weight']
                for w in range(int(weight)):
                    new_ps.append(p)
            # 从权重列表中选择一个项目
            pr = random.choice(new_ps)
            work_hour = {
                "xmbh": pr['xmbh'],
                "xmmc": pr['xmmc'],
                "rwbh": pr['rwbh'],
                "rwmc": pr['rwmc'],
                "gs": str(pr['gs']),
                "nr": pr['nr'],
                "fid": '',
                "shzt": '0'
            }
        self.fill_work_hour(day, work_hour)

    def fill_work_hour(self, day=datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d'), work_hour_dict=None):
        logger.debug("填报工时")
        if work_hour_dict is not None:
            work_hour_dict['fid'] = ''
            if 'xm' in work_hour_dict:
                del work_hour_dict['xm']
            if 'id' in work_hour_dict:
                del work_hour_dict['id']
            if 'weight' in work_hour_dict:
                del work_hour_dict['weight']
        # TODO 是否补填 5月未否，非本月为是?
        sfbt = "Y"
        data = {"wid": "", "rq": day, "ygbh": self.user_id, "ygxm": self.user_name,
                "SJY": "PC", "SFBT": sfbt, "xmGsList": [
                work_hour_dict]}
        data_str = json.dumps(data, ensure_ascii=False)
        response = httpUtil.send_post_request(
            "http://my.wisedu.com/jygl2/sys/gsxt/api/updateGs.do?"
            "data={}".format(data_str), cookies={self.app_session_key: self.mod_auth_cas})
        logger.debug(response.text)
        ret = json.loads(response.text)
        result = {
            "code": 0,
            "msg": "工时填报成功",
            "data": ret
        }
        if ret['status'] == 200:
            logger.debug("工时填报成功")
        else:
            logger.debug("工时填报失败")
            result['code'] = -1
            result['msg'] = ret['message']
        return result

    def get_unfilled_workdays(self, start_time=datetime.datetime.strftime(
        datetime.datetime.now() - datetime.timedelta(days=60), '%Y-%m-%d'),
                              end_time=datetime.datetime.now().strftime('%Y-%m-%d')):
        """查询未填报的工时日期 默认30天前到今天"""
        logger.debug("查询未填报的工时日期")
        logger.debug("开始时间：{}".format(start_time))
        logger.debug("结束时间：{}".format(end_time))
        query_string = '[{{"name": "RQ", "caption": "日期", "builder": "lessEqual", "linkOpt": "AND", "builderList": "cbl_Other","value": "{}"}},' \
                       '{{"name": "RQ", "caption": "日期","linkOpt": "AND", "builderList": "cbl_String", "builder": "moreEqual","value": "{}"}},' \
                       '{{"name": "YGBH", "value": "{}", "linkOpt": "AND", "builder": "equal"}}]'.format(end_time,
                                                                                                         start_time,
                                                                                                         self.user_id)
        params = {
            "YGBH": self.user_id,
            "querySetting": query_string,
            "pageSize": 40,
            "pageNumber": 1
        }
        response = httpUtil.send_post_request(
            "http://my.wisedu.com/jygl2/sys/gsxt/modules/wdgs/V_GS_MYGS_QUERY.do",
            params=params, cookies={self.app_session_key: self.mod_auth_cas})

        logger.debug(response.text)
        logger.debug("结束查询我的工时")
        data = json.loads(response.text)
        if data['code'] == "0":
            all_days = self.__get_days_from_start_to_end(start_time, end_time)
            holidays = self.config['holidays']
            work_hours = data['datas']['V_GS_MYGS_QUERY']['rows']
            filled_days = self.__get_filled_days(work_hours)
            work_days = set(all_days).difference(set(holidays))
            un_filled_days = list(work_days.difference(set(filled_days)))
            un_filled_days1 = self.__get_un_filled_days_before_today(un_filled_days)
            return sorted(un_filled_days1)
        raise BException("接口调用失败：{}".format(data))

    def __get_filled_days(self, work_hours):
        ws = work_hours
        # ws = list(filter(lambda h: h['ZT'] == "1", work_hours))
        ws = list(map(lambda h: h['RQ'], ws))
        return ws

    def __get_days_from_start_to_end(self, start_time, end_time):
        dateTime_start = datetime.datetime.strptime(start_time, '%Y-%m-%d')
        dateTime_end = datetime.datetime.strptime(end_time, '%Y-%m-%d')
        all_days = []
        cur_day = dateTime_start
        while True:
            cur_day = cur_day + datetime.timedelta(days=1)
            if cur_day.__gt__(dateTime_end):
                break
            all_days.append(datetime.datetime.strftime(cur_day, '%Y-%m-%d'))
        return all_days

    def __get_un_filled_days_before_today(self, un_filled_days: list):
        return list(filter(lambda day: datetime.datetime.strptime(day, '%Y-%m-%d').__lt__(datetime.datetime.now()),
                           un_filled_days))

    def audit_work_hour(self):
        """审核工时"""
        logger.debug("审核工时")
        page_size = 10
        page_number = 1
        size = self.get_need_audit_hours_size()
        if size > 0:
            count = (size // page_size) + 1
            for c in range(count):
                self._audit_work_hour(page_size, page_number)
        logger.debug("结束审核工时")

    def get_need_audit_hours_size(self):
        logger.debug("查询待审核工时个数")
        response = self._list_need_audit_work_hour(1, 1)
        if response.status_code == 200:
            ret = json.loads(response.text)
            wddb_ = ret['datas']['wddb']
            size_ = wddb_['totalSize']
            logger.debug("totalSize:{}".format(size_))
            return size_
        raise BException('接口异常:' + response.text)

    def _list_need_audit_work_hour(self, page_size=10, page_number=1):
        params = {
            "SHR": '%' + self.user_id + '%',
            "querySetting": '[{"name":"SHR","value":"%' + self.user_id + '%","linkOpt":"AND","builder":"equal"}]',
            "pageSize": page_size,
            "pageNumber": page_number
        }
        return httpUtil.send_post_request(
            "http://my.wisedu.com/jygl2/sys/gsxt/modules/wddb/wddb.do", params,
            cookies={self.app_session_key: self.mod_auth_cas})

    def _audit_work_hour(self, page_size=10, page_number=1):
        logger.debug("查询待审核工时 page_size：{}，page_size：{}".format(page_size, page_number))
        response = self._list_need_audit_work_hour(page_size, page_number)
        if response.status_code == 200:
            ret = json.loads(response.text)
            wddb_ = ret['datas']['wddb']
            wait_audit_hours = wddb_['rows']
            wids = list(map(lambda wait_audit_hour: wait_audit_hour['WID'], wait_audit_hours))
            if len(wids) == 0:
                logger.debug("待审核工单长度为0")
                return
            wids_str = ",".join(wids)
            data = '{"wid":"' + wids_str + '","ygbh":"01116284","shjg":"Y","shyj":""}'
            response1 = httpUtil.send_post_request(
                "http://my.wisedu.com/jygl2/sys/gsxt/api/auditGs.do",
                "data={}".format(data), cookies={self.app_session_key: self.mod_auth_cas})
            if response1.status_code == 200:
                ret1 = json.loads(response1.text)
                if ret1['status'] == 200:
                    logger.debug("工时审核成功")
                else:
                    logger.debug("工时审核失败")
                return

    def get_my_program(self):
        """查询我参与的项目"""
        page_size = 100
        page_number = 1
        logger.debug("查询我参与的项目 page_size：{}，page_size：{}".format(page_size, page_number))
        params = {
            "YGBH":  self.user_id,
            "pageSize": page_size,
            "pageNumber": page_number
        }
        resp = httpUtil.send_post_request(
            "http://my.wisedu.com/jygl2/sys/gsxt/modules/wdgs/V_GS_MY_XM_QUERY.do", params,
            cookies={self.app_session_key: self.mod_auth_cas})
        ret = []
        if resp.status_code == 200:
            ret1 = json.loads(resp.text)
            if ret1['code'] == "0":
                ret = ret1['datas']['V_GS_MY_XM_QUERY']['rows']
        return ret
