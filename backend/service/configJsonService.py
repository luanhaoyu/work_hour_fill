# -*- coding: utf-8 -*-
from backend.util import configJsonUtil


class ConfigJsonService:

    def __init__(self):
        self.config = configJsonUtil.get_config()
