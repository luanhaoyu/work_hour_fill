# -*- coding: utf-8 -*-
from backend.service.configJsonService import ConfigJsonService
from backend.util import configJsonUtil, stringUtil


class ProgramService(ConfigJsonService):

    def __init__(self):
        ConfigJsonService.__init__(self)

    def list_program(self):
        return self.config['programs']

    def save_program(self, program: dict):
        program['gs'] = str(program['gs'])
        program['weight'] = str(program['weight'])
        if 'id' not in program:
            program['id'] = stringUtil.uid()
            self.list_program().append(program)
        else:
            index = 0
            for p in self.list_program():
                if str(p['id']) == program["id"]:
                    self.config['programs'][index] = program
                    break
                index = index + 1
        configJsonUtil.save_json(self.config)

    def del_program(self, id):
        index = 0
        for p in self.list_program():
            if str(p['id']) == id:
                del self.config['programs'][index]
                break
            index = index + 1
        configJsonUtil.save_json(self.config)
