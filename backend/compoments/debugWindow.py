# -*- coding: utf-8 -*-
from PyQt5.QtCore import Qt

from backend.compoments.mainWindowImpl import MainWindowImpl
from backend.compoments.webView import MyQWebEngineView
from backend.util import fileUtil, configJsonUtil, argumentUtil
from backend.util.loggerFactory import Logger

logger = Logger()


class DebugWindow(MainWindowImpl):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.move(((self._main_screen_geometry.width() - self.width()) / 2) + 20,
                  ((self._main_screen_geometry.height() - self.height()) / 2) + 20)
        self.setWindowTitle("调试页面")
        self.setWindowIconText("调试页面")
        self.label.setText("调试页面")
        self.web_view = MyQWebEngineView(self.body_widget)
        self.main_window_resize_signal.connect(self.web_view.resize_web_view)
        self.index_url = "http://127.0.0.1:9223/"
        self.web_view.load_url(self.index_url)
