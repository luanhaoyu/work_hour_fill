# -*- coding: utf-8 -*-
from PyQt5.QtGui import QIcon


class AppIcon(QIcon):

    def __init__(self):
        super().__init__(":/image/image/work.png")
