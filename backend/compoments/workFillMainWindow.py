# -*- coding: utf-8 -*-
from PyQt5.QtCore import Qt

from backend.compoments.mainWindowImpl import MainWindowImpl
from backend.compoments.webView import MyQWebEngineView
from backend.util import fileUtil, configJsonUtil, argumentUtil
from backend.util.loggerFactory import Logger

logger = Logger()


class WorkFillMainWindow(MainWindowImpl):

    def __init__(self, parent=None, is_hidden=False):
        super().__init__(parent)
        self.setWindowTitle("金智教育工时助手")
        self.setWindowIconText("金智教育工时助手")
        self.label.setText("金智教育工时助手")
        self.web_view = MyQWebEngineView(self.body_widget)
        self.main_window_resize_signal.connect(self.web_view.resize_web_view)
        self.index = configJsonUtil.get_config()['base']['index']
        if argumentUtil.is_dev():
            self.index_url = self.index['dev']
        else:
            self.index_url = fileUtil.getRootPath() + self.index['prod']
        self.web_view.load_url(self.index_url)
        if is_hidden:
            self.hide()
