# -*- coding: utf-8 -*-
from PyQt5 import QtGui
from PyQt5.QtCore import Qt, QPoint, pyqtSignal, QSize
from PyQt5.QtGui import QCursor
from PyQt5.QtWidgets import QWidget, QApplication

from backend.compoments.icon import AppIcon
from backend.compoments.mainWindow import Ui_Form
from backend.util.loggerFactory import Logger

logger = Logger()


class MainWindowImpl(QWidget, Ui_Form):
    main_window_resize_signal = pyqtSignal(QSize)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        self.bPressFlag = False
        # self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)
        self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint)

        self.icon = AppIcon()
        self.setWindowIcon(self.icon)

        _desktop = QApplication.desktop()
        self._main_screen_geometry = _desktop.screenGeometry(0)
        self.init_height = self.height()
        self.init_width = self.width()
        self.move((self._main_screen_geometry.width() - self.width()) / 2,
                  (self._main_screen_geometry.height() - self.height()) / 2)
        self.init_geo = self.geometry()

        self.__init_size()

        self.__init_title_bar_event()

        self.show()

    def __init_title_bar_event(self):
        self.pushButton_min.clicked.connect(lambda: self.hide())
        self.pushButton_restore.clicked.connect(self.__init_size)
        self.pushButton_max.clicked.connect(self.__set_max)
        self.pushButton_close.clicked.connect(self.hide)

    def __init_size(self):
        self.pushButton_restore.setHidden(True)
        self.pushButton_max.setHidden(False)
        self.setGeometry(self.init_geo)
        self.main_window_resize_signal.emit(self.size())

    def get_view_act_height(self):
        return self.init_geo.height() - self.title.height() - self.menu_bar.height() - self.tabWidget.tabBar().height() + 15

    def __set_max(self):
        self.pushButton_restore.setHidden(False)
        self.pushButton_max.setHidden(True)

        self.setGeometry(self._main_screen_geometry)
        self.main_window_resize_signal.emit(self.size())

    def mousePressEvent(self, event):
        """在Qt程序中，当隐藏掉窗体的标题栏之后，如果不重写鼠标移动事件，我们是无法通过鼠标任意拖拽窗体的。 """
        self.bPressFlag = True
        self.beginDrag = event.pos()
        super().mousePressEvent(event)

    def mouseDoubleClickEvent(self, event: QtGui.QMouseEvent):
        if self.pushButton_restore.isHidden():
            self.__set_max()
        else:
            self.__init_size()
        super().mouseDoubleClickEvent(event)

    def mouseReleaseEvent(self, event):
        self.bPressFlag = False
        super().mouseReleaseEvent(event)

    def mouseMoveEvent(self, event):
        if self.beginDrag:
            qPoint = QPoint(QCursor.pos() - self.beginDrag)
            self.move(qPoint)
        super().mouseMoveEvent(event)

    def __resize_view(self, view):
        view.setGeometry(0, 0, self.init_geo.width(), self.get_view_act_height())
