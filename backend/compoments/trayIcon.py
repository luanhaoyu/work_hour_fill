# -*- coding: utf-8 -*-
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QMenu, QSystemTrayIcon, QAction

from backend.compoments.debugWindow import DebugWindow
from backend.compoments.icon import AppIcon
from backend.compoments.messageBox import MyQMessageBox
from backend.util import startUpUtil
from backend.util.globalContext import Context


class TrayIcon(QSystemTrayIcon):
    def __init__(self, mainWindowImpl=None):
        super(TrayIcon, self).__init__()
        self.mainWindowImpl = mainWindowImpl
        self.showMenu()
        self.icon = AppIcon()
        self.setIcon(self.icon)
        self.activated[QSystemTrayIcon.ActivationReason].connect(self.iconActivated)
        self.show()

    def showMenu(self):
        """设计托盘的菜单"""
        self.menu = QMenu()
        a1 = QAction("最小化", self.menu, triggered=self.mainWindowImpl.showMinimized)

        self.menu.addAction(a1)

        a2 = QAction("打开调试页面", self, triggered=self.open_debug)
        self.menu.addAction(a2)

        a3 = QAction("退出", self, triggered=self.quit)
        self.menu.addAction(a3)
        self.setContextMenu(self.menu)

    def open_debug(self):
        if Context().debugWindow is not None:
            Context().debugWindow.show()
        else:
            Context().debugWindow = DebugWindow()

    def quit(self):
        box = MyQMessageBox(text="是否确定退出？").add_ok_btn().add_cancel_btn()
        box.exec_()
        if box.clickedButton() == box.get_ok_btn():
            """完整的退出"""
            startUpUtil.stop_self()

    def iconActivated(self, QSystemTrayIcon_ActivationReason):
        if QSystemTrayIcon_ActivationReason == QSystemTrayIcon.DoubleClick:
            self.mainWindowImpl.show()
            self.mainWindowImpl.activateWindow()



    def hideTrayIcon(self, title="开始退出"):
        # self.showMessage("提示", title, self.icon, msecs=1000)
        self.setVisible(False)
