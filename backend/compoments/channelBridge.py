# -*- coding: utf-8 -*-
import _thread
import json

from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot

# 导入该目录所有router模块
from backend.exception.exceptions import BException
from backend.router.routeContext import rc
from backend.util.loggerFactory import Logger

logger = Logger()
class ChannelBridge(QObject):
    callbackSignal = pyqtSignal(str, str)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.view = parent
        self.callbackSignal.connect(self.js_callback)

    @pyqtSlot(str, result=str)
    def qt_service(self, options, router_context=None):
        if router_context is None:
            router_context = rc
        dic_op = json.loads(options)
        mode = self.thread_mode(dic_op)
        if mode == "ui":
            # print("使用ui线程处理，可以直接操作ui，但不要执行耗时操作，否则会导致ui卡顿")
            self.router(dic_op, router_context)
        else:
            # print("进入子线程处理，不能直接操作ui，要操作ui请使用 threadUtil.move_to_ui_thread ,可以进行耗时操作，不会导致ui卡顿,默认为子线程模式")
            _thread.start_new_thread(self.router, (dic_op, router_context))
        return ""

    def router(self, dic_op, router_context):
        if 'callback' not in dic_op:
            raise OSError("未设置回调函数")
        callback = dic_op['callback']
        if 'path' not in dic_op:
            print('未设置对应请求路由')
            self.callbackSignal.emit(callback, '未设置对应请求路由')
            return
        params = dic_op['params'] if 'params' in dic_op else None
        try:
            ret = router_context.serve(dic_op['path'], params)
            if ret is None:
                ret = {
                    "code": 0,
                    "msg": '',
                    "data": None
                }
            result = {
                "code": ret['code'] if 'code' in ret else -1,
                "msg": ret['msg'] if 'msg' in ret else 'error',
                "data": ret
            }
        except Exception as e:
            import traceback
            logger.error(traceback.format_exc())
            result = {
                "code": -1,
                "msg": "{}".format(e).replace("'", " ").replace("(", " ").replace(")", " "),
                "data": None
            }
        self.callbackSignal.emit(callback, json.dumps(result, ensure_ascii=False))

    def js_callback(self, callback, params):
        self.view.page().runJavaScript(callback + "('" + params + "')")

    def thread_mode(self, options):
        if "threadMode" in options:
            return options["threadMode"]
        return "child"
