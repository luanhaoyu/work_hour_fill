# -*- coding: utf-8 -*-
import os

from backend.util import jsonUtil, fileUtil
from backend.util.loggerFactory import Logger

logger = Logger()

config_path = fileUtil.getRootPath() + os.sep + 'config.json'


class CF(object):
    config = {}

    def __init__(self):
        self.config = jsonUtil.read_file(config_path)
        logger.info("config:{}".format(self.config))


cf = CF()


def get_config():
    return cf.config


def get_config_account():
    return cf.config['account']


def save_json(conf=None):
    if conf is None:
        conf = {}
    cf.config = conf
    jsonUtil.write_json(cf.config, config_path)
