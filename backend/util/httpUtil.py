# -*- coding: utf-8 -*-
import requests

from backend.util.loggerFactory import Logger

session = requests.session()

headers = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Language": "en-US,en;q=0.5",
    "Connection": "keep-alive",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/24.0",
    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
}

logger = Logger()


def send_get_request(url, params=None, cookies=None, verify=False, allow_redirects=False):
    return send_request(url, 'get', params, None, cookies, verify, headers, allow_redirects=allow_redirects)


def send_post_request(url, params=None, data=None, cookies=None, verify=False,
                      allow_redirects=False):
    if data is not None:
        content_headers = {"Content-Length": str(len(data))}
        new_headers = {**headers, **content_headers}
    else:
        new_headers = headers
    return send_request(url, 'post', params, data, cookies, verify, new_headers, allow_redirects=allow_redirects)


def send_request(url, method='get', params=None, data=None, cookies=None, verify=False, header=None,
                 allow_redirects=False):
    requests.packages.urllib3.disable_warnings()
    logger.info("url: {} ,method: {}".format(url, method))
    if params is not None:
        logger.info("params: %s" % params)
    if data is not None:
        logger.info("data: %s" % data)
    if cookies is not None:
        logger.info("cookies: %s" % cookies)
    ret = session.request(method, url, params=params, data=data, headers=header,
                          cookies=cookies, verify=verify, allow_redirects=allow_redirects)
    logger.info("status_code: %s" % ret.status_code)
    if ret.text is not None:
        if len(ret.text) <500:
            logger.info("body: %s" % ret.text)
        else:
            logger.info("body size>500: %s" % ret.text[0:500])
    else:
        logger.info("body: is None")
    return ret
