# -*- coding: utf-8 -*-
import os
import sys

import winshell
from PyQt5.QtCore import QCoreApplication

from backend.util import fileUtil
from backend.util.globalContext import Context

LINK_PATH = os.path.join(os.path.join(winshell.startup()), "work_hour_fill.lnk")
exe_ = fileUtil.getRootPath() + os.sep + 'work_hour_fill.exe'


def create_shot_cut():
    winshell.CreateShortcut(
        Path=LINK_PATH,
        Target=exe_,
        Icon=(exe_, 0),
        Description='金智教育工时助手')


def cancel_create_shot_cut():
    if os.path.exists(LINK_PATH):
        fileUtil.delFile(LINK_PATH)


def stop_self():
    from backend.util.loggerFactory import Logger
    logger = Logger()
    logger.info("退出程序")
    context = Context()
    if context.mainWindowImpl is not None:
        context.mainWindowImpl.close()
    if context.debugWindow is not None:
        context.debugWindow.close()
    if context.trayIcon is not None:
        context.trayIcon.hideTrayIcon()
    QCoreApplication.instance().quit()
    sys.exit(0)
