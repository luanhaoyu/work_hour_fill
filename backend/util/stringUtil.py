# -*- coding: utf-8 -*-
import random
import re
import uuid

randomstr = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678'


def get_first_matching_group_by_pattern(string, reg):
    # re.findall("hiddenimports=\[(.*)\],.*hookspath=", "111111111hiddenimports=[123],1111hookspath=")
    matchObj = re.findall(reg, string)
    if len(matchObj) > 0:
        return matchObj[0]
    return ''


def random_string(length=16):
    return ''.join(random.sample(randomstr, length))


def replace_first_matching_group_by_pattern(string, reg):
    return ''


def uid():
    return str(uuid.uuid4())
