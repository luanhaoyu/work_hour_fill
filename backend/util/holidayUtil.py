# -*- coding: utf-8 -*-
import time

import arrow

from backend.util import configJsonUtil


def isLeapYear(years):
    """    通过判断闰年，获取年份years下一年的总天数
    :param years: 年份，int
    :return:days_sum，一年的总天数    """
    # 断言：年份不为整数时，抛出异常。
    assert isinstance(years, int), "请输入整数年，如 2018"

    if ((years % 4 == 0 and years % 100 != 0) or (years % 400 == 0)):  # 判断是否是闰年
        # print(years, "是闰年")
        days_sum = 366
        return days_sum
    else:
        # print(years, '不是闰年')
        days_sum = 365
        return days_sum


def getAllDayPerYear(years):
    '''
    获取一年的所有日期
    :param years:年份
    :return:全部日期列表
    '''
    start_date = '%s-1-1' % years
    a = 0
    all_date_list = []
    days_sum = isLeapYear(int(years))
    print()
    while a < days_sum:
        b = arrow.get(start_date).shift(days=a).format("YYYY-MM-DD")
        a += 1
        all_date_list.append(b)
    return all_date_list


def get_holidays():
    """获取节假日"""
    # 获取一年的所有日期
    all_date_list = getAllDayPerYear("2021")
    # 获取休息日
    workend = [dstr for dstr in all_date_list if time.strptime(dstr, '%Y-%m-%d').tm_wday > 4]
    buban = [
        "2021-02-07",
        "2021-02-20",
        "2021-04-25",
        "2021-05-08",
        "2021-09-18",
        "2021-09-26",
        "2021-10-09"
    ]
    # 获取法定节假日 TODO 参考 https://gitee.com/web/holidays_api?_from=gitee_search
    legal_holidays = [
        "2021-01-01",
        "2021-01-02",
        "2021-01-03",
    ]
    # 固定周末+法定节假日-补班
    result = list(set(workend).union(set(legal_holidays)).difference(set(buban)))
    result.sort()
    print(result)

    # todo 写到configJson
    return result


def today_is_holiday():
    config = configJsonUtil.get_config()
    holidays = config['holidays']
    now = time.strftime("%Y-%m-%d", time.localtime())
    return now in holidays
