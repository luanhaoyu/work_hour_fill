#!/usr/bin/env python
# coding=utf8

# 当前文件的路径
import datetime
import hashlib
import sys
import time
import zipfile


def getRootPath():
    pwd = sys.argv[0]
    if os.path.isfile(pwd):
        pwd = os.path.abspath(os.path.dirname(pwd))
        return pwd
    return pwd


def getDirname(path):
    return os.path.dirname(path)


def getParentPath():
    return os.path.abspath(os.path.dirname(getRootPath()) + os.path.sep + ".")


def getGraderParentPath():
    return os.path.abspath(os.path.dirname(getRootPath()) + os.path.sep + "..")


def move(sourceFile, targetFile):
    from backend.util.loggerFactory import Logger
    logger = Logger()
    logger.info("移动覆盖文件（夹）：{}到：{}".format(sourceFile, targetFile))
    if os.path.exists(targetFile):
        if os.path.isfile(sourceFile):
            os.remove(targetFile)
            os.rename(sourceFile, targetFile)
    else:
        if os.path.isfile(sourceFile):
            if not os.path.exists(os.path.dirname(targetFile)):
                os.makedirs(os.path.dirname(targetFile))
            os.rename(sourceFile, targetFile)
        else:
            os.makedirs(targetFile)


def forceMove(sourcePath, targetPath, excluedFiles=None):
    """移动目录下所有文件（夹）到目标目录 文件存在则覆盖"""
    files = listFile(sourcePath)
    for f in files:
        exclude_flag = False
        if excluedFiles is not None:
            for e in excluedFiles:
                if os.path.abspath(f).startswith(os.path.abspath(e)):
                    print("排除的文件：{}".format(f))
                    exclude_flag = True
                    continue
        if not exclude_flag:
            t = targetPath + f.replace(sourcePath, "")
            move(f, t)


def listFile(rootDir):
    files = []
    if not os.path.exists(rootDir):
        return files
    __listDir(rootDir, files)
    return files


def __listDir(rootDir, files):
    for filename in os.listdir(rootDir):
        pathname = os.path.join(rootDir, filename)
        files.append(pathname)
        if not os.path.isfile(pathname):
            __listDir(pathname, files)


def delFile(filePath):
    """文件被占用无法使用os.remove删除"""
    cmd = "del /f/q  \"%s\"" % filePath
    os.system(cmd)


def delDir(path, excluedFile=None, is_del_root=True):
    if not os.path.exists(path):
        return
    filelist = os.listdir(path)  # 列出该目录下的所有文件名
    for f in filelist:
        if excluedFile is not None and f == os.path.basename(excluedFile):
            continue
        filepath = os.path.join(path, f)  # 将文件名映射成绝对路劲
        if os.path.isfile(filepath):  # 判断该文件是否为文件或者文件夹
            os.remove(filepath)  # 若为文件，则直接删除
        elif os.path.isdir(filepath):
            shutil.rmtree(filepath, True)  # 若为文件夹，则删除该文件夹及文件夹内所有文件
    if is_del_root:
        shutil.rmtree(path, True)  # 最后删除总文件夹


def unZipFile(zipFilePath, targetDir):
    """解压文件，
     :param zipFilePath:压缩文件路径
     :param targetDir:目标文件夹
     :return:
     """
    with zipfile.ZipFile(zipFilePath, 'r') as f:
        for fn in f.namelist():
            """解压会出现中文乱码"""
            f.extract(fn, targetDir)
    fix_garbled_folder(targetDir)


def fix_garbled_folder(dir_names):
    """修复乱码文件夹"""
    os.chdir(dir_names)
    for temp_name in os.listdir('.'):
        try:
            # 使用cp437对文件名进行解码还原
            new_name = temp_name.encode('cp437').decode("gbk")
            if new_name != temp_name:
                # 对乱码的文件名及文件夹名进行重命名
                os.rename(temp_name, new_name)
                # 传回重新编码的文件名给原文件名
                temp_name = new_name
        except:
            # utf-8编码不做处理
            pass
        if os.path.isdir(temp_name):
            # 对子文件夹进行递归调用
            fix_garbled_folder(temp_name)
            # 记得返回上级目录
            os.chdir('..')


def mkdir(path):
    # 去除首位空格
    path = path.strip()
    # 去除尾部 \ 符号
    path = path.rstrip("\\")
    # 判断路径是否存在
    isExists = os.path.exists(path)

    # 判断结果
    if not isExists:
        # 如果不存在则创建目录
        os.makedirs(path)
        return True
    else:
        return False


def getAbsPathByDate(tempFolderNameEnum, fileName):
    """按年月，日获取文件路径"""
    now = time.localtime(time.time())
    year_month = time.strftime('%Y%m', now)
    day = time.strftime('%d', now)
    path = tempFolderNameEnum.value + os.path.sep + year_month + os.path.sep + day + os.path.sep + fileName
    return os.path.abspath(path)


def delExpireFolders(tempFolderNameEnum, backup_count_day):
    """删除指定日期前创建的文件夹和文件"""

    del_files = listFile(tempFolderNameEnum.value)
    for file in del_files:
        #  存在 且is folder 并且父目录不是临时文件夹根目录
        flag = os.path.abspath(os.path.dirname(file) + os.path.sep + ".") == os.path.abspath(tempFolderNameEnum.value)
        if os.path.exists(file) and not os.path.isfile(file) and not flag:
            year_month_folder = os.path.basename(os.path.abspath(os.path.dirname(file) + os.path.sep + "."))
            date = year_month_folder[0:4] + "-" + year_month_folder[4:6] + "-" + os.path.basename(file)
            time = datetime.datetime.strptime(date, '%Y-%m-%d')
            # 获取backup_count_day前日期
            delDay = datetime.datetime.now() - datetime.timedelta(days=backup_count_day)
            delDayStartTIme = datetime.datetime.strptime(delDay.strftime('%Y-%m-%d'), '%Y-%m-%d')
            if time < delDayStartTIme:
                # 删除之前
                delDir(file)


def get_file_md5(filepath):
    # 获取文件的md5
    if not os.path.isfile(filepath):
        return
    myhash = hashlib.md5()
    f = open(filepath, "rb")
    while True:
        b = f.read(8096)
        if not b:
            break
        myhash.update(b)
    f.close()
    return myhash.hexdigest()


def copyFile(src, dest):
    if os.path.exists(dest):
        delFile(dest)
    return shutil.copyfile(src, dest)


def delExpireLogFile(file, configDay):
    """
    :param file: 文件路径
    :param configDay: 文件保存天数
    :return:
    """
    import re
    result = re.search(r'\d{4}-\d{2}-\d{2}', file, 0)
    if result is not None:
        date = datetime.datetime.strptime(result.group(), "%Y-%m-%d")
        """获取当前时间和日志文件时间相差的天数"""
        retDays = (datetime.datetime.now() - date).days
        if retDays > configDay:
            delFile(file)


def create_file(filename, default_content=None):
    """
    创建文件
    :param filename:
    :param default_content:
    :return:
    """
    path = filename[0:filename.rfind("/")]
    if not os.path.isdir(path):  # 无文件夹时创建
        os.makedirs(path)
    if not os.path.isfile(filename):  # 无文件时创建
        with open(filename, mode="w", encoding="utf-8") as f:
            if default_content is not None:
                f.write(default_content)
    else:
        pass


import os
import shutil


def copy_dir(sourceDir, targetDir, copyFileCounts=0):
    print(sourceDir)
    print(u"%s 当前处理文件夹%s已处理%s 个文件" % (
        time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())), sourceDir, copyFileCounts))
    for f in os.listdir(sourceDir):
        sourceF = os.path.join(sourceDir, f)
        targetF = os.path.join(targetDir, f)
        if os.path.isfile(sourceF):
            # 创建目录
            if not os.path.exists(targetDir):
                os.makedirs(targetDir)
            copyFileCounts += 1
            # 文件不存在，或者存在但是大小不同，覆盖
            if not os.path.exists(targetF) or (
                    os.path.exists(targetF) and (get_file_md5(targetF) != get_file_md5(sourceF))):
                # 2进制文件
                open(targetF, "wb").write(open(sourceF, "rb").read())
                print(u"%s %s 复制完毕" % (time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())), targetF))
            else:
                print(
                    u"%s %s 已存在，不重复复制" % (time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())), targetF))
        if os.path.isdir(sourceF):
            copy_dir(sourceF, targetF, copyFileCounts)


if __name__ == "__main__":
    pass
