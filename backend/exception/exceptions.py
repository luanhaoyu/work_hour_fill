# -*- coding: utf-8 -*-

class BException(Exception):
    pass


class AuthenticationException(BException):
    pass
