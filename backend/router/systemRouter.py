# -*- coding: utf-8 -*-
from backend.router.routeContext import rc
from backend.service.systemService import SystemService

systemService = SystemService()


@rc.route('/system/get_account')
def get_account():
    return systemService.get_account()


@rc.route('/system/save_account')
def save_account(account: dict):
    return systemService.save_account(account)


@rc.route('/system/reset_account')
def reset_account():
    return systemService.reset_account()


@rc.route('/system/get_fill_config')
def get_fill_config():
    return systemService.get_fill_config()


@rc.route('/system/save_fill_config')
def save_fill_config(fill_config: dict):
    return systemService.save_fill_config(fill_config)


@rc.route('/system/get_version')
def get_version():
    return systemService.get_version()
