# -*- coding: utf-8 -*-
from backend.router.routeContext import rc
from backend.service.programService import ProgramService
from backend.service.workHourService import WorkHourService

programService = ProgramService()


@rc.route('/program/list')
def list():
    return programService.list_program()


@rc.route('/program/save')
def save_program(program: dict):
    return programService.save_program(program)


@rc.route('/program/del')
def del_program(params: dict):
    return programService.del_program(params['id'])


@rc.route('/workHour/get_my_program')
def get_my_program():
    workHourService = WorkHourService()
    return workHourService.get_my_program()
