# -*- coding: utf-8 -*-
from backend.exception.exceptions import BException


class RouteContext:
    def __init__(self):
        self.routes = {}

    def route(self, route_str):

        def decorator(f):
            self.routes[route_str] = f
            return f

        return decorator

    def serve(self, path, params=None):
        view_function = self.routes.get(path)
        if view_function:
            if params is None or len(params) == 0:
                return view_function()
            else:
                return view_function(params)
        else:
            raise BException('Route "{}"" has not been registered'.format(path))


rc = RouteContext()
