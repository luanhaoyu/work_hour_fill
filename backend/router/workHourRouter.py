# -*- coding: utf-8 -*-
from backend.router.routeContext import rc
from backend.service.systemService import SystemService
from backend.service.workHourService import WorkHourService


@rc.route('/workHour/fill_work_hour')
def fill_work_hour(params: dict):
    workHourService = WorkHourService()
    return workHourService.fill_work_hour(params['day'], work_hour_dict=params['work_hour'])


@rc.route('/workHour/get_unfilled_workdays')
def get_unfilled_workdays(params: dict = None):
    workHourService = WorkHourService()
    if params is None:
        return workHourService.get_unfilled_workdays()
    return workHourService.get_unfilled_workdays(start_time=params['start_time'] if 'start_time' in params else None,
                                                 end_time=params['end_time'] if 'end_time' in params else None)

