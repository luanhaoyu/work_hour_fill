# -*- coding: utf-8 -*-
from backend.router.routeContext import rc


@rc.route('/hello')
def hello():
    return {"code": 0, "msg": "hello"}
