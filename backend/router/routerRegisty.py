# -*- coding: utf-8 -*-


""" router注册器 不要随意删除！"""
import importlib

from backend.util import configJsonUtil
from backend.util.loggerFactory import Logger

logger = Logger()


def load_modules():
    routers = get_modules()
    for path in routers:
        logger.debug("加载router：" + path)
        importlib.import_module(path)


def get_modules():
    return configJsonUtil.get_config()['routers']
